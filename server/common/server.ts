import "reflect-metadata";
import express from 'express';
import {Application} from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import http from 'http';
import os from 'os';
import cookieParser from 'cookie-parser';
import installValidator from './openapi';
import jwt from "@ticmas/express-jwt";
import l from './logger';
import {rateLimiterMiddleware} from "../../src/port/api/middlewares/ratelimiter";
import cors from 'cors';

const app = express();

export default class ExpressServer {
    constructor() {
        const root = path.normalize(__dirname + '/../..');
        app.set('appPath', root + 'client');
        app.use(bodyParser.json({limit: process.env.REQUEST_LIMIT || '100kb'}));
        app.use(bodyParser.urlencoded({extended: true, limit: process.env.REQUEST_LIMIT || '100kb'}));
        app.use(bodyParser.text({limit: process.env.REQUEST_LIMIT || '100kb'}));
        const options:cors.CorsOptions = {
            allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
            credentials: true,
            methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
            origin: process.env.CORS_ORIGIN,
            preflightContinue: false
        };
        app.use(cors(options));
        app.use(cookieParser(process.env.SESSION_SECRET));
        if (!process.env.hasOwnProperty('TEST_MODE')) {
            app.use(rateLimiterMiddleware);
            app.use(jwt({secret: process.env.JWT_SECRET}).unless(function (req) {
                let swaggerRoute = req.originalUrl.match("/api-explorer/");
                let specRoute = req.originalUrl.match(/^\/api\/v1\/spec$/);
                return swaggerRoute !== null || specRoute !== null;
            }));
        }
        app.use(express.static(`${root}/public`));
    }

    router(routes: (app: Application) => void): ExpressServer {
        installValidator(app, routes)
        return this;
    }

    listen(p: string | number = process.env.PORT): Application {
        const welcome = port => () => l.info(`up and running in ${process.env.NODE_ENV || 'development'} @: ${os.hostname()} on port: ${port}}`);
        http.createServer(app).listen(p, welcome(p));
        return app;
    }
}
