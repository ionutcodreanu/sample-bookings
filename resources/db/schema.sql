CREATE TABLE `bookings` (
	`aId` INT(11) NOT NULL AUTO_INCREMENT,
	`id` varchar(100) NOT NULL,
	`profileId` varchar(100) NOT NULL,
	`packageId` varchar(100) NOT NULL,
	`price` float NOT NULL,
	`status` varchar(100) NOT NULL,
	`reservedAt` DATE NOT NULL,
	PRIMARY KEY (`aId`),
	UNIQUE KEY `id` (`id`),
	UNIQUE KEY `profileId` (`profileId`),
	UNIQUE KEY `packageId` (`packageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=UTF8MB4;