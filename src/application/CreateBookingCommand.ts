export type CreateBookingCommand = {
    profileId: string,
    packageId: string,
    price: number,
}