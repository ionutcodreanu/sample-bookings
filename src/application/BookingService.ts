import {BookingsRepository} from "../domain/bookingsRepository";
import {Booking} from "../domain/booking";
import {CreateBookingCommand} from "./CreateBookingCommand";
import {TYPES} from "../port/types";
import {inject, injectable} from "inversify";

@injectable()
export class BookingService {
    private readonly bookingRepository: BookingsRepository;

    public constructor(@inject(TYPES.BookingsRepository) bookingRepository: BookingsRepository) {
        this.bookingRepository = bookingRepository;
    }

    all(): Promise<Booking[]> {
        return this.bookingRepository.all();
    }

    create(createBookingCommand: CreateBookingCommand): Promise<Booking> {
        const profile = new Booking(
            createBookingCommand.profileId,
            createBookingCommand.packageId,
            createBookingCommand.price,
        );
        return this.bookingRepository.create(profile);
    }

    byId(bookingId: string): Promise<Booking> {
        return this.bookingRepository.byId(bookingId);
    }

    delete(bookingId: string): Promise<void> {
        return this.bookingRepository.byId(bookingId).then(
            booking => this.bookingRepository.delete(booking)
        );
    }
}