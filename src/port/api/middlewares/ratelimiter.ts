import {RateLimiterMemory, IRateLimiterStoreOptions} from "rate-limiter-flexible";

const opts = {
    points: parseInt(process.env.RATE_LIMITER_POINTS),
    duration: parseInt(process.env.RATE_LIMITER_DURATION),
};

const rateLimiter = new RateLimiterMemory(opts);

export const rateLimiterMiddleware = (req, res, next) => {
    rateLimiter.consume(req.ip)
        .then(() => {
            next();
        })
        .catch(() => {
            res.status(429).json({error: 'Too Many Requests'});
        });
};
