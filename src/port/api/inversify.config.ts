import { Container } from "inversify";
import { TYPES } from "../types";
import {BookingService} from "../../application/BookingService";
import {BookingsRepository} from "../../domain/bookingsRepository";
import {InMemoryRepository} from "../persistence/InMemoryRepository";
import {BookingsController} from "./controllers/BookingsController";
import Knex from "knex";
import {MySqlRepository} from "../persistence/MySqlRepository";

const appContainer = new Container();
appContainer.bind<Knex>(TYPES.MySqlConnection).toConstantValue(Knex({
    client: "mysql2",
    connection: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE
    },
}));

appContainer.bind<BookingsRepository>(TYPES.BookingsRepository).to(MySqlRepository);
appContainer.bind<BookingService>(TYPES.BookingsService).to(BookingService);
appContainer.bind<BookingsController>(TYPES.BookingsController).to(BookingsController);

export { appContainer };