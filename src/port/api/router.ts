import express from 'express';
import {appContainer} from "./inversify.config";
import {TYPES} from "../types";
import {BookingsController} from "./controllers/BookingsController";

const controller = appContainer.get<BookingsController>(TYPES.BookingsController);
export default express.Router()
    .post('/', (req, res) => controller.create(req, res))
    .get('/', (req, res) => controller.all(req, res))
    .get('/:id', (req, res) => controller.byId(req, res))
    .delete ('/:id', (req, res) => controller.delete(req, res));