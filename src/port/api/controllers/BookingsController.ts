import {Request, Response} from 'express';
import {BookingService} from "../../../application/BookingService";
import {CreateBookingCommand} from "../../../application/CreateBookingCommand";
import {Booking} from "../../../domain/booking";
import {inject, injectable} from "inversify";
import {TYPES} from "../../types";

@injectable()
export class BookingsController {
    private readonly bookingService: BookingService;

    constructor(@inject(TYPES.BookingsService) packageService: BookingService) {
        this.bookingService = packageService;
    }

    all(req: Request, res: Response): void {
        this.bookingService.all().then(response =>
            res.status(200)
                .json(response.map(profile => this.mapBooking(profile)))
        ).catch(reason => res.status(500));
    }

    create(req: Request, res: Response): void {
        const command: CreateBookingCommand = {
            packageId: req.body.packageId,
            profileId: req.body.profileId,
            price: req.body.price,
        };
        this.bookingService.create(command).then(
            response => res.status(201)
                .json(this.mapBooking(response))
        );
    }

    byId(req: Request, res: Response): void {
        const bookingId: string = req.params['id'];
        this.bookingService.byId(bookingId)
            .then(response => res.status(200).json(this.mapBooking(response)))
            .catch(reason => res.status(404).json({}));
    }

    delete(req: Request, res: Response): void {
        const bookingId: string = req.params['id'];
        this.bookingService.delete(bookingId).then(
            () => res.status(200).json({})
        ).catch(reason => res.status(404).json());
    }

    mapBooking(booking: Booking): object {
        return {
            id: booking.id,
            profileId: booking.profileId,
            packageId: booking.packageId,
            status: booking.status,
            price: booking.price,
            reservedAt: booking.reservedAt
        };
    }
}
