import * as Knex from "knex";
import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {v4 as uuid} from "uuid";
import {BookingsRepository} from "../../domain/bookingsRepository";
import {Booking} from "../../domain/booking";

@injectable()
export class MySqlRepository implements BookingsRepository {
    private static readonly tableName = "bookings";
    private dbConnection: Knex;

    constructor(@inject(TYPES.MySqlConnection) dbConnection: Knex) {
        this.dbConnection = dbConnection;
    }

    all(): Promise<Booking[]> {
        return this.dbConnection
            .select("*")
            .from<Booking>(MySqlRepository.tableName)
            .orderByRaw("aId ASC")
            .then(result => Promise.resolve(result.map(row => this.mapRow(row))))
            .catch(reason => Promise.reject(reason));
    }

    byId(id: string): Promise<Booking> {
        return this.dbConnection<Booking>(MySqlRepository.tableName)
            .where('id', id)
            .first()
            .then(booking => this.mapRow(booking))
            .catch(reason => Promise.reject(reason));
    }

    create(booking: Booking): Promise<Booking> {
        booking.id = uuid();
        return this.dbConnection
            .into(MySqlRepository.tableName)
            .insert({
                    id: booking.id,
                    profileId: booking.profileId,
                    packageId: booking.packageId,
                    price: booking.price,
                    status: booking.status,
                    reservedAt: booking.reservedAt,
                }
            )
            .then(result => {
                return this.byId(booking.id);
            })
            .catch(reason => {
                return Promise.reject(reason)
            });
    }

    delete(booking: Booking): Promise<void> {
        return this.dbConnection(MySqlRepository.tableName)
            .where('id', booking.id)
            .del();
    }

    private mapRow(row: any): Booking {
        let booking = new Booking(
            row.profileId,
            row.packageId,
            row.price
        );
        booking.id = row.id;
        booking.status = row.status;
        booking.reservedAt = row.reservedAt;
        return booking;
    }
}