import {v4 as uuid} from "uuid";
import {BookingsRepository} from "../../domain/bookingsRepository";
import {Booking} from "../../domain/booking";
import {injectable} from "inversify";

@injectable()
export class InMemoryRepository implements BookingsRepository {
    private localStorage: Booking[] = [];

    all(): Promise<Booking[]> {
        return Promise.resolve(this.localStorage);
    }

    byId(id: string): Promise<Booking> {
        const bookingModel = this.localStorage.find(row => row.id == id);
        if (bookingModel) {
            return Promise.resolve(bookingModel);
        } else {
            return Promise.reject("Package not found");
        }
    }

    create(bookingModel: Booking): Promise<Booking> {
        bookingModel.id = uuid();
        this.localStorage.push(bookingModel);
        return Promise.resolve(bookingModel);
    }

    update(bookingModel: Booking): Promise<Booking> {
        this.localStorage.map(resource => this.localStorage.find(current => current.id === bookingModel.id) || bookingModel);
        return Promise.resolve(bookingModel);
    }

    delete(resourceToDelete: Booking): Promise<void> {
        this.localStorage = this.localStorage.filter(currentResource => currentResource.id != resourceToDelete.id);
        return Promise.resolve();
    }
}