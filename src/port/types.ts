const TYPES = {
    BookingsService: Symbol.for("BookingsService"),
    BookingsRepository: Symbol.for("BookingsRepository"),
    BookingsController: Symbol.for("BookingsController"),
    MySqlRepository: Symbol.for("MySqlRepository"),
    MySqlConnection: Symbol.for("MySqlConnection")
};

export {TYPES};