import {Booking} from "./booking";

export interface BookingsRepository {
    create(profile: Booking): Promise<Booking>;

    all(): Promise<Booking[]>;

    byId(id: string): Promise<Booking>;

    delete(profile: Booking): Promise<void>;
}