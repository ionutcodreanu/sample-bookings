import {Location} from "./location";

export class Booking {
    protected static readonly STATUS_NEW = "new";
    private _id: string;
    private _reservedAt: string;
    private _profileId: string;
    private _packageId: string;
    private _price: number;
    private _status: string;

    constructor(profileId: string, packageId: string, price: number) {
        this._profileId = profileId;
        this._packageId = packageId;
        this._price = price;
        this._reservedAt = new Date().toLocaleDateString();
        this._status = Booking.STATUS_NEW;
    }

    get id(): string {
        return this._id;
    }

    set id(value: string) {
        this._id = value;
    }

    get reservedAt(): string {
        return this._reservedAt;
    }

    get profileId(): string {
        return this._profileId;
    }

    get packageId(): string {
        return this._packageId;
    }

    get price(): number {
        return this._price;
    }

    get status(): string {
        return this._status;
    }

    set reservedAt(value: string) {
        this._reservedAt = value;
    }

    set status(value: string) {
        this._status = value;
    }
}