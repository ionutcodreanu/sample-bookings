import 'mocha';
import request from 'supertest';
import Server from '../server';
import * as assert from "assert";
import * as faker from "faker";

describe('Bookings test cases', () => {

    let bookingToBeCreated = createBooking();
    let token = "token";

    let lastId: string = "";

    function assertResourceIsSame(actualResource, expectedResource) {
        assert.strictEqual(actualResource.profileId, expectedResource.profileId);
        assert.strictEqual(actualResource.packageId, expectedResource.packageId);
        assert.strictEqual(actualResource.price, parseFloat(expectedResource.price));
        assert.strictEqual(actualResource.status, "new");
    }

    it("should be able to create a booking", async () => {
        let response = await request(Server)
            .post('/api/v1/bookings')
            .set('Authorization', 'Bearer ' + token)
            .send(bookingToBeCreated)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(201, response.statusCode);
        assertResourceIsSame(response.body, bookingToBeCreated);
        lastId = response.body.id;
    });

    it("would create many bookings", async () => {
        let booking1 = await request(Server)
            .post('/api/v1/bookings')
            .set('Authorization', 'Bearer ' + token)
            .send(createBooking());
        assert.equal(booking1.statusCode, 201);
        let booking2 = await request(Server)
            .post('/api/v1/bookings')
            .set('Authorization', 'Bearer ' + token)
            .send(createBooking());
        assert.equal(booking2.statusCode, 201);
        let booking3 = await request(Server)
            .post('/api/v1/bookings')
            .set('Authorization', 'Bearer ' + token)
            .send(createBooking());
        assert.equal(booking3.statusCode, 201);
        let booking4 = await request(Server)
            .post('/api/v1/bookings')
            .set('Authorization', 'Bearer ' + token)
            .send(createBooking());
        assert.equal(booking4.statusCode, 201);
        let booking5 = await request(Server)
            .post('/api/v1/bookings')
            .set('Authorization', 'Bearer ' + token)
            .send(createBooking());
        assert.equal(booking5.statusCode, 201);
    });

    it("should be able to delete a booking", async () => {
            let response = await request(Server)
                .get('/api/v1/bookings')
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(response.statusCode, 200);
            assert.equal(response.body.length, 6);
            let packageToDelete = response.body[3];
            let deletedPackageResponse = await request(Server)
                .delete('/api/v1/bookings/' + packageToDelete.id)
                .set('Authorization', 'Bearer ' + token)
                .set('Content-Type', 'application/json')
                .set('Accept', 'application/json');
            assert.equal(deletedPackageResponse.statusCode, 200);
        }
    );

    it("should fetch less profiles after the booking was deleted", async () => {
        let response = await request(Server)
            .get('/api/v1/bookings')
            .set('Authorization', 'Bearer ' + token)
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
        assert.equal(response.statusCode, 200);
        assert.equal(response.body.length, 5);
    });

    function createBooking() {
        return {
            profileId: faker.random.uuid(),
            packageId: faker.random.uuid(),
            price: faker.finance.amount(800, 2500)
        };
    }
});
