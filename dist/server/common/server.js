"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const body_parser_1 = __importDefault(require("body-parser"));
const http_1 = __importDefault(require("http"));
const os_1 = __importDefault(require("os"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const openapi_1 = __importDefault(require("./openapi"));
const express_jwt_1 = __importDefault(require("@ticmas/express-jwt"));
const logger_1 = __importDefault(require("./logger"));
const ratelimiter_1 = require("../../src/port/api/middlewares/ratelimiter");
const app = express_1.default();
class ExpressServer {
    constructor() {
        const root = path_1.default.normalize(__dirname + '/../..');
        app.set('appPath', root + 'client');
        app.use(body_parser_1.default.json({ limit: process.env.REQUEST_LIMIT || '100kb' }));
        app.use(body_parser_1.default.urlencoded({ extended: true, limit: process.env.REQUEST_LIMIT || '100kb' }));
        app.use(body_parser_1.default.text({ limit: process.env.REQUEST_LIMIT || '100kb' }));
        app.use(cookie_parser_1.default(process.env.SESSION_SECRET));
        if (!process.env.hasOwnProperty('TEST_MODE')) {
            app.use(ratelimiter_1.rateLimiterMiddleware);
            app.use(express_jwt_1.default({ secret: process.env.JWT_SECRET }).unless(function (req) {
                let swaggerRoute = req.originalUrl.match("/api-explorer/");
                let specRoute = req.originalUrl.match(/^\/api\/v1\/spec$/);
                return swaggerRoute !== null || specRoute !== null;
            }));
        }
        app.use(express_1.default.static(`${root}/public`));
    }
    router(routes) {
        openapi_1.default(app, routes);
        return this;
    }
    listen(p = process.env.PORT) {
        const welcome = port => () => logger_1.default.info(`up and running in ${process.env.NODE_ENV || 'development'} @: ${os_1.default.hostname()} on port: ${port}}`);
        http_1.default.createServer(app).listen(p, welcome(p));
        return app;
    }
}
exports.default = ExpressServer;
//# sourceMappingURL=server.js.map