"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TYPES = {
    BookingsService: Symbol.for("BookingsService"),
    BookingsRepository: Symbol.for("BookingsRepository"),
    BookingsController: Symbol.for("BookingsController"),
    MySqlRepository: Symbol.for("MySqlRepository"),
    MySqlConnection: Symbol.for("MySqlConnection")
};
exports.TYPES = TYPES;
//# sourceMappingURL=types.js.map