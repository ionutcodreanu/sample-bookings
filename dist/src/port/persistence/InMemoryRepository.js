"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
const inversify_1 = require("inversify");
let InMemoryRepository = class InMemoryRepository {
    constructor() {
        this.localStorage = [];
    }
    all() {
        return Promise.resolve(this.localStorage);
    }
    byId(id) {
        const bookingModel = this.localStorage.find(row => row.id == id);
        if (bookingModel) {
            return Promise.resolve(bookingModel);
        }
        else {
            return Promise.reject("Package not found");
        }
    }
    create(bookingModel) {
        bookingModel.id = uuid_1.v4();
        this.localStorage.push(bookingModel);
        return Promise.resolve(bookingModel);
    }
    update(bookingModel) {
        this.localStorage.map(resource => this.localStorage.find(current => current.id === bookingModel.id) || bookingModel);
        return Promise.resolve(bookingModel);
    }
    delete(resourceToDelete) {
        this.localStorage = this.localStorage.filter(currentResource => currentResource.id != resourceToDelete.id);
        return Promise.resolve();
    }
};
InMemoryRepository = __decorate([
    inversify_1.injectable()
], InMemoryRepository);
exports.InMemoryRepository = InMemoryRepository;
//# sourceMappingURL=InMemoryRepository.js.map