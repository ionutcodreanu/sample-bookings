"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var MySqlRepository_1;
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("../types");
const uuid_1 = require("uuid");
const booking_1 = require("../../domain/booking");
let MySqlRepository = MySqlRepository_1 = class MySqlRepository {
    constructor(dbConnection) {
        this.dbConnection = dbConnection;
    }
    all() {
        return this.dbConnection
            .select("*")
            .from(MySqlRepository_1.tableName)
            .orderByRaw("aId ASC")
            .then(result => Promise.resolve(result.map(row => this.mapRow(row))))
            .catch(reason => Promise.reject(reason));
    }
    byId(id) {
        return this.dbConnection(MySqlRepository_1.tableName)
            .where('id', id)
            .first()
            .then(booking => this.mapRow(booking))
            .catch(reason => Promise.reject(reason));
    }
    create(booking) {
        booking.id = uuid_1.v4();
        return this.dbConnection
            .into(MySqlRepository_1.tableName)
            .insert({
            id: booking.id,
            profileId: booking.profileId,
            packageId: booking.packageId,
            price: booking.price,
            status: booking.status,
            reservedAt: booking.reservedAt,
        })
            .then(result => {
            return this.byId(booking.id);
        })
            .catch(reason => {
            return Promise.reject(reason);
        });
    }
    delete(booking) {
        return this.dbConnection(MySqlRepository_1.tableName)
            .where('id', booking.id)
            .del();
    }
    mapRow(row) {
        let booking = new booking_1.Booking(row.profileId, row.packageId, row.price);
        booking.id = row.id;
        booking.status = row.status;
        booking.reservedAt = row.reservedAt;
        return booking;
    }
};
MySqlRepository.tableName = "bookings";
MySqlRepository = MySqlRepository_1 = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.TYPES.MySqlConnection))
], MySqlRepository);
exports.MySqlRepository = MySqlRepository;
//# sourceMappingURL=MySqlRepository.js.map