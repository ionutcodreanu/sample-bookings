"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const inversify_config_1 = require("./inversify.config");
const types_1 = require("../types");
const controller = inversify_config_1.appContainer.get(types_1.TYPES.BookingsController);
exports.default = express_1.default.Router()
    .post('/', (req, res) => controller.create(req, res))
    .get('/', (req, res) => controller.all(req, res))
    .get('/:id', (req, res) => controller.byId(req, res))
    .delete('/:id', (req, res) => controller.delete(req, res));
//# sourceMappingURL=router.js.map