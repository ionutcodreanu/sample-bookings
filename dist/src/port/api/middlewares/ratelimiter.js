"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rate_limiter_flexible_1 = require("rate-limiter-flexible");
const opts = {
    points: parseInt(process.env.RATE_LIMITER_POINTS),
    duration: parseInt(process.env.RATE_LIMITER_DURATION),
};
const rateLimiter = new rate_limiter_flexible_1.RateLimiterMemory(opts);
exports.rateLimiterMiddleware = (req, res, next) => {
    rateLimiter.consume(req.ip)
        .then(() => {
        next();
    })
        .catch(() => {
        res.status(429).json({ error: 'Too Many Requests' });
    });
};
//# sourceMappingURL=ratelimiter.js.map