"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("../../types");
let BookingsController = class BookingsController {
    constructor(packageService) {
        this.bookingService = packageService;
    }
    all(req, res) {
        this.bookingService.all().then(response => res.status(200)
            .json(response.map(profile => this.mapBooking(profile)))).catch(reason => res.status(500));
    }
    create(req, res) {
        const command = {
            packageId: req.body.packageId,
            profileId: req.body.profileId,
            price: req.body.price,
        };
        this.bookingService.create(command).then(response => res.status(201)
            .json(this.mapBooking(response)));
    }
    byId(req, res) {
        const bookingId = req.params['id'];
        this.bookingService.byId(bookingId)
            .then(response => res.status(200).json(this.mapBooking(response)))
            .catch(reason => res.status(404).json({}));
    }
    delete(req, res) {
        const bookingId = req.params['id'];
        this.bookingService.delete(bookingId).then(() => res.status(200).json({})).catch(reason => res.status(404).json());
    }
    mapBooking(booking) {
        return {
            id: booking.id,
            profileId: booking.profileId,
            packageId: booking.packageId,
            status: booking.status,
            price: booking.price,
            reservedAt: booking.reservedAt
        };
    }
};
BookingsController = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.TYPES.BookingsService))
], BookingsController);
exports.BookingsController = BookingsController;
//# sourceMappingURL=BookingsController.js.map