"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const types_1 = require("../types");
const BookingService_1 = require("../../application/BookingService");
const BookingsController_1 = require("./controllers/BookingsController");
const knex_1 = __importDefault(require("knex"));
const MySqlRepository_1 = require("../persistence/MySqlRepository");
const appContainer = new inversify_1.Container();
exports.appContainer = appContainer;
appContainer.bind(types_1.TYPES.MySqlConnection).toConstantValue(knex_1.default({
    client: "mysql2",
    connection: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DATABASE
    },
}));
appContainer.bind(types_1.TYPES.BookingsRepository).to(MySqlRepository_1.MySqlRepository);
appContainer.bind(types_1.TYPES.BookingsService).to(BookingService_1.BookingService);
appContainer.bind(types_1.TYPES.BookingsController).to(BookingsController_1.BookingsController);
//# sourceMappingURL=inversify.config.js.map