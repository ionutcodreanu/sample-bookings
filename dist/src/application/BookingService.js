"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const booking_1 = require("../domain/booking");
const types_1 = require("../port/types");
const inversify_1 = require("inversify");
let BookingService = class BookingService {
    constructor(bookingRepository) {
        this.bookingRepository = bookingRepository;
    }
    all() {
        return this.bookingRepository.all();
    }
    create(createBookingCommand) {
        const profile = new booking_1.Booking(createBookingCommand.profileId, createBookingCommand.packageId, createBookingCommand.price);
        return this.bookingRepository.create(profile);
    }
    byId(bookingId) {
        return this.bookingRepository.byId(bookingId);
    }
    delete(bookingId) {
        return this.bookingRepository.byId(bookingId).then(booking => this.bookingRepository.delete(booking));
    }
};
BookingService = __decorate([
    inversify_1.injectable(),
    __param(0, inversify_1.inject(types_1.TYPES.BookingsRepository))
], BookingService);
exports.BookingService = BookingService;
//# sourceMappingURL=BookingService.js.map