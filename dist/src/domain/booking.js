"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Booking {
    constructor(profileId, packageId, price) {
        this._profileId = profileId;
        this._packageId = packageId;
        this._price = price;
        this._reservedAt = new Date().toLocaleDateString();
        this._status = Booking.STATUS_NEW;
    }
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
    get reservedAt() {
        return this._reservedAt;
    }
    get profileId() {
        return this._profileId;
    }
    get packageId() {
        return this._packageId;
    }
    get price() {
        return this._price;
    }
    get status() {
        return this._status;
    }
    set reservedAt(value) {
        this._reservedAt = value;
    }
    set status(value) {
        this._status = value;
    }
}
exports.Booking = Booking;
Booking.STATUS_NEW = "new";
//# sourceMappingURL=booking.js.map